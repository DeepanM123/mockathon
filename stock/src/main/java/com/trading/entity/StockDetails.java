package com.trading.entity;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class StockDetails implements Serializable {

	@Id
	@GeneratedValue
	private int stockId;
	private String stockName;
	private int cost;
	private int numberOfQuantity;

	@ManyToOne
	@JoinColumn(name = "stocksId")
	Stocks stocks;

	public StockDetails() {
		super();
	}

	public StockDetails(Stocks stocks) {
		super();
		this.stocks = stocks;
	}

	@JsonIgnore
	public Stocks getStocks() {
		return stocks;
	}

	public void setStocks(Stocks stocks) {
		this.stocks = stocks;
	}

	public int getStockId() {
		return stockId;
	}

	public void setStockId(int stockId) {
		this.stockId = stockId;
	}

	public String getStockName() {
		return stockName;
	}

	public void setStockName(String stockName) {
		this.stockName = stockName;
	}

	public int getCost() {
		return cost;
	}

	public void setCost(int cost) {
		this.cost = cost;
	}

	public int getNumberOfQuantity() {
		return numberOfQuantity;
	}

	public void setNumberOfQuantity(int numberOfQuantity) {
		this.numberOfQuantity = numberOfQuantity;
	}

}
