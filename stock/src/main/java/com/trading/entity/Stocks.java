package com.trading.entity;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Stocks implements Serializable {

	@Id
	@GeneratedValue
	private int stocksId;
	private String stocksName;

	public Stocks() {
		super();
	}

	public int getStocksId() {
		return stocksId;
	}

	public void setStocksId(int stocksId) {
		this.stocksId = stocksId;
	}

	public String getStocksName() {
		return stocksName;
	}

	public void setStocksName(String stocksName) {
		this.stocksName = stocksName;
	}

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "stocks")
	Set<StockDetails> stock;

	public Stocks(Set<StockDetails> stock) {
		super();
		this.stock = stock;
	}

	@JsonIgnore
	public Set<StockDetails> getStock() {
		return stock;
	}

	public void setStock(Set<StockDetails> stock) {
		this.stock = stock;
	}

}
