package com.trading.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.trading.entity.Stocks;

@Repository
public interface StocksRepository extends JpaRepository<Stocks, Integer> {

	

}
