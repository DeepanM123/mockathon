package com.trading.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.trading.entity.StockDetails;

@Repository
public interface StockDetailRepository extends JpaRepository<StockDetails, Integer> {

	List<StockDetails> findAllByStocks_stocksId(int stocksid);


	Optional<StockDetails> findByStocks_stocksIdAndStockName(int stockid, String stockName);

}
