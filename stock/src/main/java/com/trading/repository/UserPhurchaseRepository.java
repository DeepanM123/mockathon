package com.trading.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.trading.entity.UserPhurchase;

@Repository
public interface UserPhurchaseRepository extends JpaRepository<UserPhurchase, Integer> {



	Optional<UserPhurchase> findByUserid(int userid);

}
