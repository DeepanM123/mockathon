package com.trading.dto;

public class UserPhurchaseDto {
	private int userid;
	private String userName;
	private String email;
	private String phonenumber;
	private int stockid;
	private int quantity;
	private String stockName;
	
	
	public UserPhurchaseDto() {
		super();
	}
	
	public UserPhurchaseDto(int userid, String userName, String email, String phonenumber, int stockid, int quantity,
			String stockName) {
		super();
		this.userid = userid;
		this.userName = userName;
		this.email = email;
		this.phonenumber = phonenumber;
		this.stockid = stockid;
		this.quantity = quantity;
		this.stockName = stockName;
	}

	public String getStockName() {
		return stockName;
	}
	public void setStockName(String stockName) {
		this.stockName = stockName;
	}
	public int getUserid() {
		return userid;
	}
	public void setUserid(int userid) {
		this.userid = userid;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhonenumber() {
		return phonenumber;
	}
	public void setPhonenumber(String phonenumber) {
		this.phonenumber = phonenumber;
	}
	public int getStockid() {
		return stockid;
	}
	public void setStockid(int stockid) {
		this.stockid = stockid;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
	

}
