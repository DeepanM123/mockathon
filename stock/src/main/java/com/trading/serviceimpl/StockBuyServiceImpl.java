package com.trading.serviceimpl;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.trading.dto.UserPhurchaseDto;
import com.trading.entity.StockDetails;
import com.trading.entity.UserPhurchase;
import com.trading.exception.TradingException;
import com.trading.repository.StockDetailRepository;
import com.trading.repository.UserPhurchaseRepository;
import com.trading.service.StockBuyService;
import com.trading.utility.ErrorConstant;

@Service
public class StockBuyServiceImpl implements StockBuyService {

	@Autowired
	StockDetailRepository stockDetailRepository;

	@Autowired
	UserPhurchaseRepository userpurchaserepository;

	@Override
	public String saveData(@Valid UserPhurchaseDto userpurchasedto) throws TradingException {
		Optional<StockDetails> stockdetails = stockDetailRepository.findByStocks_stocksIdAndStockName(userpurchasedto.getStockid(),userpurchasedto.getStockName());
		if (!stockdetails.isPresent()) {
			throw new TradingException(ErrorConstant.RECORD_NOT_FOUND);
		}

		if ((stockdetails.get().getNumberOfQuantity() > 0)
				&& (stockdetails.get().getNumberOfQuantity() >= stockdetails.get().getNumberOfQuantity())) {

			stockdetails.get()
					.setNumberOfQuantity(stockdetails.get().getNumberOfQuantity() - userpurchasedto.getQuantity());
			int totalcost = (stockdetails.get().getCost()) * (userpurchasedto.getQuantity());
			stockDetailRepository.save(stockdetails.get());
			UserPhurchase userpurchase = new UserPhurchase();
			BeanUtils.copyProperties(userpurchasedto, userpurchase);
			userpurchase.setCost(totalcost);
			userpurchaserepository.save(userpurchase);
		}

		return "Stock Bought successfully";
	}

	@Override
	public List<UserPhurchase> getAllUsers() {
		return userpurchaserepository.findAll();
	}

}
