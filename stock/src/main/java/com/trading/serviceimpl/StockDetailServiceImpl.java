package com.trading.serviceimpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.trading.entity.StockDetails;
import com.trading.exception.TradingException;
import com.trading.repository.StockDetailRepository;
import com.trading.service.StockDetailService;
import com.trading.utility.ErrorConstant;

@Service
public class StockDetailServiceImpl implements StockDetailService {

	@Autowired
	StockDetailRepository stockDetailRepository;

	@Override
	public List<StockDetails> getallstock(int stocksId) throws TradingException {
		List<StockDetails> stock = stockDetailRepository.findAllByStocks_stocksId(stocksId);

		if (!stock.isEmpty()) {
			// &&stocks.get(0).getUser().getUserId()!=0
			return stock;
		}
		throw new TradingException(ErrorConstant.RECORD_NOT_FOUND);
	}
}
