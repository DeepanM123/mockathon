package com.trading.serviceimpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.trading.entity.Stocks;
import com.trading.repository.StocksRepository;
import com.trading.service.StocksService;

@Service
public class StocksServiceImpl implements StocksService {

	@Autowired
	StocksRepository stocksrepository;

	@Override
	public List<Stocks> getAllStocks() {

		return stocksrepository.findAll();
	}

}
