package com.trading.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.trading.dto.UserPhurchaseDto;
import com.trading.entity.StockDetails;
import com.trading.entity.Stocks;
import com.trading.entity.UserPhurchase;
import com.trading.exception.TradingException;
import com.trading.service.StockBuyService;
import com.trading.service.StockDetailService;
import com.trading.service.StocksService;

@RestController
public class TradingController {

	@Autowired
	StockDetailService stockDetailService;
	@Autowired
	StocksService stocksService;
	@Autowired
	StockBuyService stockBuyService;

	@GetMapping("/Stocks")
	public List<Stocks> getallStocks() {
		return stocksService.getAllStocks();
	}

	@GetMapping("/Stocks/{stocksId}")
	public List<StockDetails> getallstockbystocksid(@PathVariable int stocksId) throws TradingException {
		return stockDetailService.getallstock(stocksId);

	}

	@PostMapping("/stockPurchase")
	public ResponseEntity<String> purchasestock(@Valid @RequestBody UserPhurchaseDto userpurchasedto)
			throws TradingException {

		return new ResponseEntity<String>(stockBuyService.saveData(userpurchasedto), HttpStatus.OK);
	}

	@GetMapping("/mystocks")
	public List<UserPhurchase> getAllUserPurchase() {
		return stockBuyService.getAllUsers();
	}
     
	
}
