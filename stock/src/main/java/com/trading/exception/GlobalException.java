package com.trading.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.trading.utility.ErrorConstant;

@ControllerAdvice
public class GlobalException extends ResponseEntityExceptionHandler {

	@ExceptionHandler(TradingException.class)
	public ResponseEntity<ErrorResponse> error(TradingException ex) {
		ErrorResponse er = new ErrorResponse();

		er.setMessage(ex.getMessage());

		er.setStatusCode(ErrorConstant.RECORD_NOT_FOUND_STATUS_CODE);

		return new ResponseEntity<ErrorResponse>(er, HttpStatus.NOT_ACCEPTABLE);

	}

}