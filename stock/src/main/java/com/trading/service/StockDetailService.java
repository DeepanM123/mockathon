package com.trading.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.trading.entity.StockDetails;
import com.trading.exception.TradingException;

@Service
public interface StockDetailService {

	List<StockDetails> getallstock(int stockid) throws TradingException;

}
