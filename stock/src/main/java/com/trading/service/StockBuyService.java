package com.trading.service;

import java.util.List;

import javax.validation.Valid;

import org.springframework.stereotype.Service;

import com.trading.dto.UserPhurchaseDto;
import com.trading.entity.UserPhurchase;
import com.trading.exception.TradingException;

@Service
public interface StockBuyService {

	String saveData(@Valid UserPhurchaseDto userpurchasedto) throws TradingException;

	List<UserPhurchase> getAllUsers();

}
