package com.trading.service;

import java.util.List;


import org.springframework.stereotype.Service;

import com.trading.entity.Stocks;

@Service
public interface StocksService {

	List<Stocks> getAllStocks();

}
