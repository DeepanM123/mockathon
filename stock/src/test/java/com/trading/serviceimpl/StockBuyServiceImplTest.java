package com.trading.serviceimpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.trading.dto.UserPhurchaseDto;
import com.trading.entity.StockDetails;
import com.trading.entity.Stocks;
import com.trading.entity.UserPhurchase;
import com.trading.exception.TradingException;
import com.trading.repository.StockDetailRepository;
import com.trading.repository.UserPhurchaseRepository;


@RunWith(MockitoJUnitRunner.class)
public class StockBuyServiceImplTest {

	@Mock 
	StockDetailRepository stockDetailRepository;

	@Mock
	UserPhurchaseRepository userpurchaserepository;
	
	@InjectMocks
	StockBuyServiceImpl stockbuyserviceimpl;
	
	List<Stocks> listofstocks;
	List<StockDetails> stockdetails;
	List<UserPhurchase> userpurchase;
	List<UserPhurchaseDto> userpurchasedto;
	List<UserPhurchaseDto> userpurchasedto1;

	Stocks stocks;
	StockDetails stockDetails;
	UserPhurchase userPhurchase;
	UserPhurchaseDto userpurchased;

	Set<StockDetails> StockDetailsSets;
	int stockid;

	@org.junit.Before
	public void setup() {
		stocks = new Stocks();
		stocks.setStock(StockDetailsSets);
		stocks.setStocksId(1);
		stocks.setStocksName("ss");
		listofstocks = new ArrayList<>();
		listofstocks.add(stocks);

		stockDetails = new StockDetails();
		stockDetails.setCost(1);
		stockDetails.setNumberOfQuantity(1);
		stockDetails.setStockId(1);
		stockDetails.setStockName("ee");
		stockDetails.setStocks(stocks);
		stockdetails = new ArrayList<>();
		stockdetails.add(stockDetails);
		stockid = 1;

		userPhurchase = new UserPhurchase();
		userPhurchase.setCost(1);
		userPhurchase.setEmail("ee");
		userPhurchase.setId(1);
		userPhurchase.setPhonenumber("222222222");
		userPhurchase.setQuantity(1);
		userPhurchase.setStockid(1);
		userPhurchase.setUserid(1);
		userPhurchase.setUserName("eee");
		userPhurchase.setStockName("bmw");
		userpurchase = new ArrayList<>();
		userpurchase.add(userPhurchase);

		userpurchased = new UserPhurchaseDto();
		userpurchased.setEmail("ee");
		userpurchased.setPhonenumber("1234");
		userpurchased.setQuantity(1);
		userpurchased.setStockid(1);
		userpurchased.setUserid(1);
		userpurchased.setUserName("eee");
		userpurchased.setStockName("bmw");
		userpurchasedto = new ArrayList<>();
		userpurchasedto.add(userpurchased);

	}

	@Test
	public void getAllUsers() {

		Mockito.when(userpurchaserepository.findAll()).thenReturn(userpurchase);
		List<UserPhurchase> actual = stockbuyserviceimpl.getAllUsers();
		Assert.assertEquals(1, actual.size());
	}

	@Test
	public void buyStocks() throws TradingException {
		Mockito.when(stockDetailRepository.findAllByStocks_stocksId(Mockito.anyInt())).thenReturn(stockdetails);
		String string = stockbuyserviceimpl.saveData(userpurchased);
		Assert.assertEquals("Stock Bought successfully",string);
	}
}
