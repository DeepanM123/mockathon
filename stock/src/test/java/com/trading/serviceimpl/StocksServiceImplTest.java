package com.trading.serviceimpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.trading.dto.UserPhurchaseDto;
import com.trading.entity.StockDetails;
import com.trading.entity.Stocks;
import com.trading.entity.UserPhurchase;
import com.trading.repository.StocksRepository;

@RunWith(MockitoJUnitRunner.class)
public class StocksServiceImplTest {

	@Mock
	StocksRepository stocksrepository;

	@InjectMocks
	StocksServiceImpl stocksserviceimpl;
	
	List<Stocks> listofstocks;
	List<StockDetails> stockdetails;
	
	
	Stocks stocks;
	StockDetails stockDetails;
	
	
	Set<StockDetails> StockDetailsSets;
	int stockid;

	@org.junit.Before
	public void setup() {
		stocks = new Stocks();
		stocks.setStock(StockDetailsSets);
		stocks.setStocksId(1);
		stocks.setStocksName("ss");
		listofstocks = new ArrayList<>();
		listofstocks.add(stocks);

		stockDetails = new StockDetails();
		stockDetails.setCost(1);
		stockDetails.setNumberOfQuantity(1);
		stockDetails.setStockId(1);
		stockDetails.setStockName("ee");
		stockDetails.setStocks(stocks);
		stockdetails = new ArrayList<>();
		stockdetails.add(stockDetails);
		
		
		
	}


	@Test
	public void getAllstocks() {
		Mockito.when(stocksrepository.findAll()).thenReturn(listofstocks);
		List<Stocks> actual = stocksserviceimpl.getAllStocks();
		Assert.assertEquals(1, actual.size());

	}

}
