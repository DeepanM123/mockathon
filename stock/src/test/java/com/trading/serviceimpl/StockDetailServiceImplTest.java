package com.trading.serviceimpl;

import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.List;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.trading.entity.StockDetails;
import com.trading.entity.Stocks;
import com.trading.exception.TradingException;
import com.trading.repository.StockDetailRepository;

@RunWith(MockitoJUnitRunner.class)
public class StockDetailServiceImplTest {

	@Mock
	StockDetailRepository stockDetailRepository;
	@InjectMocks
	StockDetailServiceImpl stockdetailserviceimpl;

	List<StockDetails> stockdetails;
	List<StockDetails> stockdetails1;

	StockDetails stockDetails;
	Stocks stocks;
	int stockid;

	@org.junit.Before
	public void setup() {

		stockDetails = new StockDetails();
		stockDetails.setCost(1);
		stockDetails.setNumberOfQuantity(1);
		stockDetails.setStockId(1);
		stockDetails.setStockName("ee");
		stockDetails.setStocks(stocks);
		stockdetails = new ArrayList<>();
		stockdetails.add(stockDetails);
		stockid = 1;

	}

	@Test
	public void getStockDetails() throws TradingException {

		Mockito.when(stockDetailRepository.findAllByStocks_stocksId((Mockito.anyInt()))).thenReturn(stockdetails);
		List<StockDetails> actualValue = stockdetailserviceimpl.getallstock(stockid);
		Assert.assertEquals(1, actualValue.size());

	}

	@Test
	public void getStockDetailsNegative() throws TradingException {

		stockdetails1 = new ArrayList<>();
		StockDetails stockDetails1 = new StockDetails();
		stockdetails1.add(stockDetails1);
		int stockid = 0;
		Mockito.when(stockDetailRepository.findAllByStocks_stocksId((Mockito.anyInt()))).thenReturn(stockdetails1);
		// List<StockDetails> actualValue =
		// tradingcontroller.getallstockbystocksid(stockid);
		assertThrows(TradingException.class, () -> stockdetailserviceimpl.getallstock(stockid));

	}
}
