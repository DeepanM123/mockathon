package com.trading.controller;

import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;

import com.trading.dto.UserPhurchaseDto;
import com.trading.entity.StockDetails;
import com.trading.entity.Stocks;
import com.trading.entity.UserPhurchase;
import com.trading.exception.TradingException;
import com.trading.service.StockBuyService;
import com.trading.service.StockDetailService;
import com.trading.service.StocksService;

@RunWith(MockitoJUnitRunner.class)
public class TradingControllerTest {

	@Mock
	StockDetailService stockDetailService;
	@Mock
	StocksService stocksService;
	@Mock
	StockBuyService stockBuyService;

	@InjectMocks
	TradingController tradingcontroller;
	List<Stocks> listofstocks;
	List<StockDetails> stockdetails;
	List<StockDetails> stockdetails1;
	List<UserPhurchase> userpurchase;
	List<UserPhurchaseDto> userpurchasedto;
	List<UserPhurchaseDto> userpurchasedto1;
	
	Stocks stocks;
	StockDetails stockDetails;
	UserPhurchase userPhurchase;
	UserPhurchaseDto userpurchased;
	
	Set<StockDetails> StockDetailsSets;
	int stockid;

	@org.junit.Before
	public void setup() {
		stocks = new Stocks();
		stocks.setStock(StockDetailsSets);
		stocks.setStocksId(1);
		stocks.setStocksName("ss");
		listofstocks = new ArrayList<>();
		listofstocks.add(stocks);

		stockDetails = new StockDetails();
		stockDetails.setCost(1);
		stockDetails.setNumberOfQuantity(1);
		stockDetails.setStockId(1);
		stockDetails.setStockName("ee");
		stockDetails.setStocks(stocks);
		stockdetails = new ArrayList<>();
		stockdetails.add(stockDetails);
		stockid = 1;
		
		userPhurchase=new UserPhurchase();
		userPhurchase.setCost(1);
		userPhurchase.setEmail("ee");
		userPhurchase.setId(1);
		userPhurchase.setPhonenumber("222222222");
		userPhurchase.setQuantity(1);
		userPhurchase.setStockid(1);
		userPhurchase.setUserid(1);
		userPhurchase.setStockName("bmw");
		userPhurchase.setUserName("eee");
		userpurchase=new ArrayList<>();
		userpurchase.add(userPhurchase);
		
		userpurchased=new UserPhurchaseDto();
		userpurchased.setEmail("ee");
		userpurchased.setPhonenumber("1234");
		userpurchased.setQuantity(1);
		userpurchased.setStockid(1);
		userpurchased.setUserid(1);
		userpurchased.setUserName("eee");
		userpurchased.setStockName("bmw");
		userpurchasedto=new ArrayList<>();
		userpurchasedto.add(userpurchased);

	}

	@Test
	public void getallstocks() {
		Mockito.when(stocksService.getAllStocks()).thenReturn(listofstocks);
		List<Stocks> actual = tradingcontroller.getallStocks();
		Assert.assertEquals(1, actual.size());

	}

	@Test
	public void getStockDetails() throws TradingException {

		Mockito.when(stockDetailService.getallstock((Mockito.anyInt()))).thenReturn(stockdetails);
		List<StockDetails> actualValue = tradingcontroller.getallstockbystocksid(stockid);
		Assert.assertEquals(1, actualValue.size());

	}
	
	@Test
	public void getStockDetailsNegative() throws TradingException {

		stockdetails1=new ArrayList<>();
		StockDetails stockDetails1=new StockDetails();
		stockdetails1.add(stockDetails1);
		int stockid=0;
		Mockito.when(stockDetailService.getallstock((Mockito.anyInt()))).thenReturn(stockdetails1);
		//List<StockDetails> actualValue = tradingcontroller.getallstockbystocksid(stockid);
		assertThrows(TradingException.class,()->tradingcontroller.getallstockbystocksid(stockid));

		
	}
	
	@Test
	public void getAllUser()
	{
		Mockito.when(stockBuyService.getAllUsers()).thenReturn(userpurchase);
		List<UserPhurchase> actual = tradingcontroller.getAllUserPurchase();
		Assert.assertEquals(1, actual.size());

	}
	
	@Test
	public void buyStocks() throws TradingException
	{
		Mockito.when(stockBuyService.saveData(Mockito.any())).thenReturn("Stock Bought successfully");
		 ResponseEntity<String> string=tradingcontroller.purchasestock(userpurchased);
		 Assert.assertEquals("Stock Bought successfully","Stock Bought successfully");
	}
	
	@Test
	public void buyStocksnegative() throws TradingException
	{
		userpurchasedto1=new ArrayList<>();
		UserPhurchaseDto userPhurchaseDto1=new UserPhurchaseDto();
		userpurchasedto1.add(userPhurchaseDto1);
		Mockito.when(stockBuyService.saveData(Mockito.any())).thenReturn("Record not found");
		 //ResponseEntity<String> string=tradingcontroller.purchasestock(userpurchased);
		 assertThrows(TradingException.class,()->tradingcontroller.purchasestock(userPhurchaseDto1));
	}
	
}
