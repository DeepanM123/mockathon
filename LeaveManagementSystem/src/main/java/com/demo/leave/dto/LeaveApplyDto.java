package com.demo.leave.dto;

import java.util.Date;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import com.demo.leave.enums.LeaveType;

public class LeaveApplyDto {

	
	private int userIdfk;
	private int leaveIdfk;
	public int getUserIdfk() {
		return userIdfk;
	}
	public void setUserIdfk(int userIdfk) {
		this.userIdfk = userIdfk;
	}
	public int getLeaveIdfk() {
		return leaveIdfk;
	}
	public void setLeaveIdfk(int leaveIdfk) {
		this.leaveIdfk = leaveIdfk;
	}

	@Enumerated(EnumType.STRING)
	private LeaveType leaveType;
	private int numberOfLeaves;
	private Date FromDate;
	private Date ToDate;
	
	public LeaveType getLeaveType() {
		return leaveType;
	}
	public void setLeaveType(LeaveType leaveType) {
		this.leaveType = leaveType;
	}
	public Date getFromDate() {
		return FromDate;
	}
	public void setFromDate(Date fromDate) {
		FromDate = fromDate;
	}
	public Date getToDate() {
		return ToDate;
	}
	public void setToDate(Date toDate) {
		ToDate = toDate;
	}
	
	
	
	public int getNumberOfLeaves() {
		return numberOfLeaves;
	}
	public void setNumberOfLeaves(int numberOfLeaves) {
		this.numberOfLeaves = numberOfLeaves;
	}
	
	
	
}
