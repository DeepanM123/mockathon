package com.demo.leave.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.demo.leave.entity.Leaves;
import com.demo.leave.exception.LeaveException;

@Service
public interface LeaveService {

	List<Leaves> getallleave(int userid) throws LeaveException;

}
