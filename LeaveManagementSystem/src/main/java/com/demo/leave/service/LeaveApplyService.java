package com.demo.leave.service;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.demo.leave.dto.LeaveApplyDto;
import com.demo.leave.exception.LeaveException;

@Service
public interface LeaveApplyService {

	public String saveLeaveData(@Valid LeaveApplyDto leaveapply) throws LeaveException;

}
