package com.flight.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.flight.airlineserviceimpl.AirLineServiceImpl;
import com.flight.entity.Flight;
import com.flight.entity.Route;
import com.flight.exceptions.AirLineNotFoundException;
import com.flight.repository.FlightRepository;

import junit.framework.Assert;

@RunWith(MockitoJUnitRunner.class)
public class AirlineServiceImplTests {
	
	@Mock
	FlightRepository flightRepository;
	

	@InjectMocks
	AirLineServiceImpl airLineServiceImpl;
	
	Flight flight;
	Set<Route> route;
	String airline;
	
	
	List<Flight> listOfFlight;
	List<Flight> flight1;
	@Before
	public void setup() {
		listOfFlight=new ArrayList<>();
		flight1=new ArrayList<>();
		flight=new Flight();
		flight.setAirlines("spc");
		flight.setCapacity(4);
		flight.setFlightid(2);
		flight.setRoute(route);
		flight.setServicetype("kusumajiii");
	    listOfFlight.add(flight);
	    airline="spc";
	}
	
	@Test
	public void getFlights() throws AirLineNotFoundException
	{
		Mockito.when(flightRepository.findAllByAirlines(Mockito.anyString())).thenReturn(listOfFlight);
		List<Flight> actual=airLineServiceImpl.getallflights(airline);
		Assert.assertEquals(1, actual.size());
	}
	
	@Test(expected=AirLineNotFoundException.class)
	public void getFlight() throws AirLineNotFoundException
	{
		Mockito.when(flightRepository.findAllByAirlines(Mockito.anyString())).thenReturn(flight1);
		List<Flight> actual=airLineServiceImpl.getallflights(airline);
		Assert.assertEquals(0, actual.size());
	}
}
