package com.flight.exceptions;

public class AirLineNotFoundException  extends Exception {
	private static final long serialVersionUID = -9079454849611061074L;

	public AirLineNotFoundException() {
		super();
	}

	public AirLineNotFoundException(final String message) {
		super(message);
	}


}

