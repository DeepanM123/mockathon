package com.flight.repository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.flight.entity.FlightSchedule;

public interface FlightScheduleRepository extends JpaRepository<FlightSchedule, Integer> {


	public List<FlightSchedule> findAllByAirportidAndFlightidAndDatetimeBetween(int airportid, int flightid,
			LocalDateTime timeone, LocalDateTime timetwo);

	public Optional<FlightSchedule> findByAirportidAndFlightidAndDatetimeBetween(int sourceairportid, int flightid,
			LocalDateTime timeone, LocalDateTime timetwo);

}
