package com.flight.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.flight.entity.AirLine;

public interface AirLineRepository extends JpaRepository<AirLine,Integer>{

}
