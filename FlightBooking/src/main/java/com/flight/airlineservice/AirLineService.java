package com.flight.airlineservice;

import java.util.List;

import org.springframework.stereotype.Service;

import com.flight.entity.Flight;
import com.flight.exceptions.AirLineNotFoundException;
@Service
public interface AirLineService {
	
	public List<Flight> getallflights(String airlinename) throws AirLineNotFoundException;

}
