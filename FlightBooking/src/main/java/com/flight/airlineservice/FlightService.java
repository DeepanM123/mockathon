package com.flight.airlineservice;

import java.util.List;

import org.springframework.stereotype.Service;
import com.flight.dto.FlightScheduleRequest;
import com.flight.dto.FlightScheduleResponse;
import com.flight.exceptions.FlightNotFoundException;

@Service
public interface FlightService {
	
	
	public List<FlightScheduleResponse> findcurrentscheduledflights(FlightScheduleRequest flightScheduleRequest) throws FlightNotFoundException;

}
